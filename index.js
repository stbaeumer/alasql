'use strict'

function tabelleAnzeigen(titel, arr){
    let ausgabestring = titel +"<br>"
	let kopfzeile = ""
	for (var i = 0; i < arr.length; i++) {		
		var row = arr[i];
		ausgabestring += "<tr>"
        kopfzeile = "<tr>"
        Object.keys(row).forEach(function(column) {
            ausgabestring += "<td>"                
            ausgabestring += row[column]
            ausgabestring += "</td>"
            kopfzeile += "<td>"                
            kopfzeile += column
			kopfzeile += "</td>"
        });
        ausgabestring += "</tr>"
        kopfzeile += "</tr>"
	}
    ausgabestring = "<table border='6'>" + kopfzeile + ausgabestring + "</table>"
    return ausgabestring;
}


alasql("CREATE TABLE cities (city string, population number)");
alasql("INSERT INTO cities VALUES ('Rome',2863223), ('Paris',2249975), ('Berlin',3517424),  ('Madrid',3041579)");
var orte = alasql("SELECT * FROM cities WHERE population < 3500000 ORDER BY population DESC");
	// res now contains this array of object:
	// [{"city":"Madrid","population":3041579},{"city":"Rome","population":2863223},{"city":"Paris","population":2249975}] 	


console.log("Der erste Ort der Liste: " + orte[0]);

console.log("Das Attribut 'city' des ersten Orts: " + orte[0].city);


console.log(orte);
document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Orte:", orte);
